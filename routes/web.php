<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
use Illuminate\Support\Facades\Storage;

Route::get('/', function () {
    return view('images');
});

Route::get('/subscribe', function(){
    return view('subscribe');
});

Route::get('/posts', function(){
    return view('post');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::post('/uploads', function(){
    return  'Ok';
});
Route::get('/images', function(){

    $adapter = \Storage::disk('dropbox')->getAdapter();
    $client = $adapter->getClient();
    $videoPath = '/uploads/videos';
    $files = Storage::disk('dropbox')->getAdapter()->listContents($videoPath);
    $links = [];
    foreach($files as $file){
        $links[] = $client->getTemporaryLink($file['path']);
    }
    //dd($links);
    return view('images', compact('links'));

});
Route::get('/users/report/{view_type}', 'UserController@report');
Route::get('/pdf/{view_type}', 'FileController@pdf');

Route::get('/ticket', function(){
    return view('email.ticket');
});
