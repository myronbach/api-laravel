<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('/register', 'RegisterController@register');

/*
| JWT Auth routes
|
 * */
Route::group(['prefix' => 'auth'], function ($router) {

    Route::post('login', 'AuthController@login');
    Route::post('logout', 'AuthController@logout');
    Route::post('refresh', 'AuthController@refresh');
    Route::post('me', 'AuthController@me');
    Route::post('payload', 'AuthController@payload');

});

// Password reset routes
Route::post('password/email', 'Auth\ForgotPasswordController@getResetToken');
Route::post('password/reset', 'Auth\ResetPasswordController@reset');


Route::apiResource('posts', 'PostController');


Route::apiResource('categories', 'CategoryController');
Route::any('errors', 'CategoryController@errors');

// exceptions
Route::post('exceptions', 'FileController@testException');
// files
Route::post('images/upload', 'FileController@upload');
Route::get('images/show', 'FileController@show');
Route::get('images', 'FileController@index');

Route::post('/test', 'FileController@zip');

Route::get('/dropbox',function()
{
    $filename = 'public/image2.png';
    $adapter = \Storage::disk('dropbox')->getAdapter();
    $client = $adapter->getClient();
    $link = $client->getTemporaryLink($filename);
    return $link;
});