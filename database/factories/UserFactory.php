<?php

use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(App\User::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'email' => $faker->unique()->safeEmail,
        'password' => bcrypt('123456'), // secret
        'remember_token' => str_random(10),
    ];
});


//\Illuminate\Support\Facades\App::setLocale('en');
//$fakerLoc = \Faker\Factory::create('en_US');
//$fakerLoc = \Faker\Factory::create('uk_UA');

/*$factory->define(App\Post::class, function(Faker $faker) use ($fakerLoc){
    return [
        'published_at' => $faker->dateTime($max = 'now', $timezone = null),
        'title' => $fakerLoc->realText($maxNbChars = 250, $indexSize = 2),
        'body' => $fakerLoc->realText($maxNbChars = 250, $indexSize = 2),
    ];
});*/


$factory->define(App\Category::class, function(Faker $faker){
    return [

    ];
});
