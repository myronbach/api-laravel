<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = factory(App\User::class,3)
            ->create()
            ->each(function($user){
                $user->posts()->save(factory(App\Post::class)->make());
            });
    }
}
