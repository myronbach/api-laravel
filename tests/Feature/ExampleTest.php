<?php

namespace Tests\Feature;

use App\Post;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ExampleTest extends TestCase
{
    use RefreshDatabase;
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testBasicTest()
    {

        $first = factory(Post::class)->create([
            'title' => 'First post',
            'published_at' => \Carbon\Carbon::now()
        ]);
        $second = factory(Post::class)->create([
            'title' => 'Second post',
            'published_at' => \Carbon\Carbon::now()->subMonth()
        ]);


        $posts = Post::where('title', 'First post')->get();

        $this->assertCount(1, $posts);

    }
}
