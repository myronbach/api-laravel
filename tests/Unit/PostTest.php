<?php

namespace Tests\Unit;

use App\Post;
//use PHPUnit\Framework\TestCase;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class PostTest extends TestCase
{
    function testPostHasName()
    {
        $post = Post::find(43);
        $this->assertEquals('beer', $post->title);
    }

}
