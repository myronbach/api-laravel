<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Spatie\Translatable\HasTranslations;

class Category extends Model
{
    use HasTranslations;

    protected $guarded = ['id'];

    public $translatable = ['title', 'description'];

   /* public function getTitleAttribute($value)
    {
        return __('categories.title.'.$this->id);
    }

    public function getDescriptionAttribute($value)
    {
        return __('categories.description.'.$this->id);
    }*/



}
