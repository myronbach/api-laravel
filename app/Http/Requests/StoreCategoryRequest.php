<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreCategoryRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title.en' => 'required|max:100',
            'title.*' => 'max:100',
            'description.*' => 'max:2000',
            'type' => 'integer|required',
            'level' => 'required|integer|max:10',
            'slug' => 'required|unique:categories,slug'
        ];
    }
}
