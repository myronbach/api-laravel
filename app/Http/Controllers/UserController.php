<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use PDF;
use App\User;

class UserController extends Controller
{
    public function report(Request $request)
    {
        $users = User::all();

        if($request->view_type === 'download') {
            $pdf = PDF::loadView('email.ticket', ['users' => $users]);
            return $pdf->download('users.pdf');
        } else {
            $view = View('email.ticket', ['users' => $users]);
            $pdf = \App::make('dompdf.wrapper');
            $pdf->loadHTML($view->render());
            return $pdf->stream();
        }

    }
}
