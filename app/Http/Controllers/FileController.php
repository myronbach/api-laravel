<?php

namespace App\Http\Controllers;

use App\Post;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use App\Services\FilesUpload\Contracts\FileUpload;
use Milon\Barcode\DNS1D;
use Zip;
use PDF;



class FileController extends Controller
{
    public function index()
    {
        //Cache::forget('previews');
        $links = Cache::remember('previews', 1, function(){
            $adapter = Storage::disk('dropbox')->getAdapter();
            $client = $adapter->getClient();
            $videoPath = '/uploads/videos';
            $files = $adapter->listContents($videoPath);
            $links = [];
            foreach($files as $file){
                $links[] = $client->getTemporaryLink($file['path']);
            }
            return $links;
        });

        //dd($links);

        return view('images', compact('links'));
    }
    public function show()
    {
        $adapter = \Storage::disk('dropbox')->getAdapter();
        $client = $adapter->getClient();
        $name = '8.jpg';
        $videoPath = '/uploads/videos/'.$name;
        return $client->getTemporaryLink($videoPath);
        //return Storage::disk('dropbox')->download('public/image2.png');

    }

    public function upload(Request $request)
    {
        $rules = [
           'image' => 'required|image|max:5000'
        ];
        $this->validate($request, $rules);
        $name = $request->file('image')->getClientOriginalName();
        $videoPath = '/uploads/videos';
        $file = $request->file('image');
        //$image = Image::make($file->getRealPath())->resize('320', null);
        //$path = Storage::disk($disk)->path($dir).'/'.$name;
        //$path = $request->file('image')->storeAs('images', $name);
        $path = Storage::disk('dropbox')->putFileAs($videoPath, $request->file('image'), $name);
        return $path;
        //return 'Ok';

    }
    public function test(FileUpload $file)
    {
        $file = $file->uploadFile('image');
        return $file;
    }

    // test Exceptions
    public function testException(Request $request)
    {
        $rules = [
            'image' => 'required|image|max:5000'
        ];
        $this->validate($request, $rules);

        $file = $request->file('image');

        try{

            $path = $this->storeImage($file);

            $post = Post::create([
                'title' => 'Post test',
                'body' => 'Paragraph 3',
                'published_at' => Carbon::now(),
                'user_id' => 1
            ]);

        } catch(\Exception $e) {
            // if DB error - delete stored file
            if(isset($path)) $this->deleteImage($path);

            return $e->getMessage();
        }

        return $path;

    }

    public function storeImage($file)
    {
        $dir = $this->getPath('_avatars');
        //dd('Ok');
        //if(!$dir) throw new \Exception('path not found');
        $name = $file->getClientOriginalName();
        //dd($dir);
        $path = Storage::disk('public')->putFileAs($dir, $file, $name);
        return $path;
    }

    public function deleteImage($path)
    {
        $path = Storage::disk('public')->delete($path);
        return $path;
    }

    public function getPath($type)
    {
        if($type == 'avatars'){
            return 'avatars';
        } else {
            throw new \Exception('path not found');
        }

    }



    public function zip(Request $request)
    {
        $file = $request->zip;
        $path = $file->storeAs('temp', $file->getClientOriginalName());
        $zip = new \ZipArchive();
        if($zip->open(Storage::disk('local')->path($path)) === TRUE){
            $zip->extractTo(Storage::path('temp/extract'));
            $zip->close();
        } else {
            return 'Error';
        }
        $files = Storage::files();


        return $files;
    }

    public function pdf(Request $request)
    {
         //echo DNS1D::getBarcodeSVG("4445645656", "C128");
         //dd('o');
        if($request->view_type === 'download') {
            $pdf = PDF::loadView('email.ticket', ['users' => 'Bill']);
            return $pdf->download('users.pdf');
        } else {
            $view = View('email.ticket', ['users' => 'Bill']);
            //PDF::setOptions(['dpi' => 150, 'defaultFont' => 'Arial']);
            $pdf = \App::make('dompdf.wrapper');
            $pdf->setOptions(['dpi' => 300]);
            $pdf->loadHTML($view->render());
            return $pdf->stream();
        }
    }


}
