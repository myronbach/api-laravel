<?php

namespace App\Http\Controllers;

use App\Category;
use App\Http\Resources\CategoryCollection as CategoryCollectionResource;
use App\Http\Resources\Category as CategoryResource;
use App\Http\Resources\CategoryCollection;
use Illuminate\Http\Request;
use App\Http\Requests\StoreCategoryRequest;
use App\Services\Translate\TranslateService;


class CategoryController extends Controller
{
    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request)
    {
        $query = Category::query();
       // filtering
        if($request->has('filter')){
            foreach($request->input('filter') as $criteria => $value){
                $query->where($criteria, $value);
            }
        }
        // sorting
        $sortCol = $request->input('sort', 'order');
        $sortDir = starts_with($sortCol, '-') ? 'desc' : 'asc';
        $sortCol = ltrim($sortCol, '-');
        $query->orderBy($sortCol, $sortDir);

        $response = $query->paginate();

        return new CategoryCollection($response);
    }

    /**
     * @param StoreCategoryRequest $request
     * @param TranslateService $translate
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(StoreCategoryRequest $request, TranslateService $translate)
    {
        $category = new Category();
        $category->title = $request->title;
        $category->description = $request->description;
        $category->type = $request->type;
        $category->level = $request->level;
        $category->order = $request->order ? : 0;
        $category->preview = $request->preview;
        $category->slug = $request->slug; //str_slug($request->title['en'], '-');
        $category->parent_id = $request->parent_id? : 0;
        $category->save();

        // add translations to files
        /*$group = 'categories';
        $lines = ['title', 'description'];
        $translate->storeTranslations($group, $lines, $request, $category->id);*/
        return response()->json($category, 201);
    }

    /**
     * @param Category $category
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response
     */
    public function show(Category $category)
    {
        if(is_null($category)){
            return response()->json(null, 404);
        }
        return new CategoryResource($category);
    }


    /**
     * @param Request $request
     * @param Category $category
     * @param TranslateService $translate
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request, Category $category, TranslateService $translate)
    {
        $category->title = $request->title['en'];
        $category->description = $request->description['en'];
        $category->type = $request->type;
        $category->order = $request->order ;
        $category->preview = $request->preview;
        $category->slug = str_slug($request->title['en'], '-');
        $category->parent_id = $request->parent_id;
        $category->save();

        // add translations to files
       /* $group = 'categories';
        $lines = ['title', 'description'];
        $translate->storeTranslations($group, $lines, $request, $category->id);*/
        return response()->json($category, 201);
    }

    /**
     * @param Category $category
     * @param TranslateService $translate
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function destroy(Category $category, TranslateService $translate)
    {
        if(is_null($category)){
            return response()->json(null, 404);
        }
        $group = 'categories';
        $lines = ['title', 'description'];
        $translate->deleteTranslations($group, $lines, $category->id);
        $category->delete();
        return response()->json(null, 204);
    }

    public function errors()
    {
        return response()->json([], 501);
    }
}
