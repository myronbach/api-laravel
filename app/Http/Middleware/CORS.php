<?php

namespace App\Http\Middleware;

use Closure;

class CORS
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $headers = [];

        // Allow from any origin
        if ($request->hasHeader('Origin')) {
            // Decide if the origin in $_SERVER['HTTP_ORIGIN'] is one
            // you want to allow, and if so:
            $headers['Access-Control-Allow-Origin'] = $request->header('Origin');
            $headers['Access-Control-Allow-Credentials'] = 'true';
        } else {
            $headers['Access-Control-Allow-Origin'] = '*';
        }
        $headers['Access-Control-Max-Age'] = '86400'; // cache for 1 day

        // Access-Control headers are received during OPTIONS requests
        if ($request->isMethod('OPTIONS')) {
            //allow methods GET, POST, OPTIONS
            if ($request->hasHeader('Access-Control-Allow-Method')) {
                // may also be using PUT, PATCH, HEAD etc
                $headers['Access-Control-Allow-Methods'] = 'GET,POST,OPTIONS';
            }
            //allow all headers
            if ($request->hasHeader('Access-Control-Request-Headers')) {
                $headers['Access-Control-Allow-Headers'] = $request->header('Access-Control-Request-Headers');
            }
            return response()->json(['method' => 'OPTIONS'], 200, $headers);
        }

        /**
         * @var $response Response
         */
        $response = $next($request);
        foreach ($headers as $key => $value) {
            $response->headers->set($key, $value);
        }

        return $response;
    }
}
