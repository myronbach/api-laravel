<?php

namespace App\Providers;

use App\Services\FilesUpload\UploadImages;
use App\Services\FilesUpload\UploadZip;
use Illuminate\Support\ServiceProvider;
use App\Services\FilesUpload\Contracts\FileUpload;

class FileServiceProvider extends ServiceProvider
{
    //protected $defer = true;
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        /*$this->app->bind(FileUpload::class, function($app){
            return new UploadZip();
        });*/

        $this->app->bind(FileUpload::class, UploadImages::class);
    }

    /*public function providers()
    {
        return [FileUpload::class];
    }*/
}
