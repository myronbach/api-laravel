<?php

namespace App;

use Laraplus\Data\Translatable;
use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    //use Translatable;
    protected $guarded = [];

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
