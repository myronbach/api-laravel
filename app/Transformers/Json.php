<?php
/**
 * Created by PhpStorm.
 * User: myron
 * Date: 12.08.18
 * Time: 23:34
 */

namespace App\Transformers;


class Json
{
    public static function response($data = null, $message = null)
    {
        return [
            'data'    => $data,
            'message' => $message,
        ];
    }
}