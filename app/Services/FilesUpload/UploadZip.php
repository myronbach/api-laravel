<?php
/**
 * Created by PhpStorm.
 * User: myron
 * Date: 23.09.18
 * Time: 23:15
 */

namespace App\Services\FilesUpload;


class UploadZip implements Contracts\FileUpload
{
    public function uploadFile($file)
    {
        return __CLASS__. ' handles ' . $file;
    }
}