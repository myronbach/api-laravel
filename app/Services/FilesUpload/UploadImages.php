<?php
/**
 * Created by PhpStorm.
 * User: myron
 * Date: 23.09.18
 * Time: 23:12
 */

namespace App\Services\FilesUpload;


class UploadImages implements Contracts\FileUpload
{
    public function uploadFile($file)
    {
        return __CLASS__. ' handles ' . $file;
    }
}