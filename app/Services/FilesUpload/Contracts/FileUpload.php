<?php
/**
 * Created by PhpStorm.
 * User: myron
 * Date: 23.09.18
 * Time: 23:10
 */

namespace App\Services\FilesUpload\Contracts;


interface FileUpload
{
    public function uploadFile($file);
}