<?php


namespace App\Services\Translate;


class TranslateService
{
    protected $manager;

    public function __construct(TranslateManager $manager)
    {
        $this->manager = $manager;
    }

    public function storeTranslations($group, array $lines,  $request, $categoryId)
    {

        $locales = config('app.locales');
        //$group = 'categories';
        //$lines = ['title', 'description'];

        foreach($lines as $line){
            $translations = [];
            foreach($locales as $key => $locale){
                $translations[$key] = $request->{$line}[$key] ? : null;
            }
            $line = $line. '.' . $categoryId;
            $this->addTranslations($group, $line, $translations);
        }
    }

    public function addTranslations($group, $line, array $translations, $htmlentities = false)
    {
        foreach($this->manager->getLanguages() as $language)
        {
            $translation = key_exists($language, $translations)? $translations[$language] : null;
            if(is_null($translation)) continue;

            $this->manager->setLanguage($language)->addLine($group, $line, $translation, $htmlentities);
        }
        return $this->manager->getLanguages();
    }

    public function deleteTranslations($group, array $lines, $categoryId)
    {
        foreach($lines as $line){
            $line = $line. '.' . $categoryId;
            $this->manager->removeLine($group, $line);
        }
    }

}