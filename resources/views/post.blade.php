<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta property="og:url"           content="http://d48a6da1.ngrok.io/posts" />
    <meta property="og:type"          content="article" />
    <meta property="og:title"         content="Your Website Title" />
    <meta property="og:description"   content="Your description" />
    <meta property="og:image"         content="http://d48a6da1.ngrok.io/storage/images/image-2.jpg" />

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    {{--<script src="{{ asset('js/app.js') }}" defer></script>--}}

    <!-- Fonts -->
    <link rel="dns-prefetch" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet" type="text/css">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/cropperjs/1.4.3/cropper.css">


</head>
<body>
<div id="app">
    <nav class="navbar navbar-expand-md navbar-light navbar-laravel">
        <div class="container">
            <a class="navbar-brand" href="{{ url('/') }}">
                {{ config('app.name', 'Laravel') }}
            </a>

        </div>
    </nav>

    <main class="py-4">
        <div class="row">
            <div class="col-md-8">
                <img src="/storage/images/image-2.jpg" alt="">
            </div>
            <div class="align-content-center">
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolor iusto nobis officia officiis quas quod vel. Accusantium architecto beatae consequuntur distinctio dolor dolore earum, impedit in magnam necessitatibus perspiciatis quod soluta ut. Adipisci alias asperiores atque dolorem error fuga minus nihil nostrum numquam obcaecati odit placeat repellendus, vel velit veritatis?

            </div>
        </div>
    </main>
</div>

<div id="fb-root"></div>
<script>(function(d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s); js.id = id;
        js.src = 'https://connect.facebook.net/uk_UA/sdk.js#xfbml=1&version=v3.2&appId=174446983119649&autoLogAppEvents=1';
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));</script>
<div class="fb-share-button" data-href="http://d48a6da1.ngrok.io/posts" data-layout="button_count" data-size="small" data-mobile-iframe="true"><a target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=http%3A%2F%2Fd48a6da1.ngrok.io%2Fposts&amp;src=sdkpreparse" class="fb-xfbml-parse-ignore">Поширити</a></div>

</body>
</html>
