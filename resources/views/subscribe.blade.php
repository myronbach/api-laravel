@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Dashboard</div>
                <div class="" id="app" >

                </div>

                <div class="card-body">
                    <form action="/subscribe" method="POST">
                        {{ csrf_field() }}
                        <input type="email" name="email"/>
                        <button type="submit">Subscribe</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>


@endsection
