@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Dashboard</div>
                <div class="" id="app" >

                </div>

                <div class="card-body">

                </div>
            </div>
            <img id="image" src="/storage/images/image1.png" alt="" style="max-width: 100%;">
        </div>
    </div>
</div>

<script>
    $.document(function(){
        var $image = $('#image');

        $image.cropper({
            aspectRatio: 16 / 9,
            crop: function(event) {
                console.log(event.detail.x);
                console.log(event.detail.y);
                console.log(event.detail.width);
                console.log(event.detail.height);
                console.log(event.detail.rotate);
                console.log(event.detail.scaleX);
                console.log(event.detail.scaleY);
            }
        });
    });


</script>
@endsection
