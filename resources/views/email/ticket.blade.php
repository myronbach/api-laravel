<style>
    body{
        margin: 0;
        padding: 0;
    }
    .ticket{
        width: 100%;
        margin: 0 auto;
    }
    .name-event-ticket{
        font-size: 100px;
        color: #000;
        font-family: "SF UI Display Medium";
        letter-spacing: 1px;
    }
    .code{
        text-align: left;
    }
    .code img{
        display: block;
        width: 380px;
        height: 163px;
    }
    .code span{
        font-size: 29px;
        font-family: "SF UI Display Medium";
        color: #000;
        letter-spacing: 1px;
        display: inline-block;
        padding: 2px 5px;
    }
    .code span:last-child{
        float: right;
    }
    .short-desc{
        font-size: 29px;
        color: #000;
        font-family: "SF UI Display Medium";
        padding: 85px 0;
    }
    .table-info-event{
        width: 100%;
        border:1px solid #000;
        border-collapse: collapse;
        margin-bottom: 160px;
    }
    .table-info-event td{
        font-size: 50px;
        color: #000;
        font-family: "SF UI Display Medium";
    }
    .table-info-event .name-event{
        font-size: 200px;
        font-family: "DCC - Ash";
        color: #000;
        width: 322px;
        padding: 0 165px;
        text-transform: uppercase;
        line-height: 0.9;
    }
    .rules td{
        width: 50%;
    }
    .rules td p{
        margin-bottom: 70px;
        font-size: 29px;
        color: #000;
        font-family: "SF UI Display Medium";
    }
    .rules tr td:first-child{
        padding-right: 30px;
    }
    .rules tr td:last-child{
        padding-left: 30px;
    }
</style>
<table class="ticket">
    <tr>
        <td class="name-event-ticket">
            Ticket <br>
            All festival
        </td>
        <td style="text-align: right">
            <div class="code" style="display: inline-block;">
                <img src="data:image/png;base64, {{DNS1D::getBarcodePNG("4445645656", "C128",3,33)}}   " alt="barcode"   />

                {{--{!! \DNS1D::getBarcodeHTML("4445645656", "C128", 3,55,"black" ,true) !!}--}}

            </div>
        </td>
    </tr>
    <tr>
        <td colspan="2" class="short-desc">
            This ticket (bar code) can only be used once. Do not pass the contents of this ticket to third parties. The ticket owner is solely responsible for preserving the electronic ticket. This ticket is incoming and is not subject to exchange.
        </td>
    </tr>
    <tr>
        <td colspan="2">
            <table class="table-info-event">
                <tr>
                    <td colspan="3" style="padding: 80px 0;"></td>
                </tr>
                <tr>
                    <td rowspan="5" class="name-event">
                        gem fest 2019
                    </td>
                    <td>Date:</td>
                    <td>12-14 July</td>
                </tr>
                <tr>
                    <td>Location:</td>
                    <td>Jurmala | Telšu iela 44</td>
                </tr>
                <tr>
                    <td>Beginning:</td>
                    <td>17:00</td>
                </tr>
                <tr>
                    <td>Name:</td>
                    <td>Emely Fanton</td>
                </tr>
                <tr>
                    <td>Price:</td>
                    <td>150 €</td>
                </tr>
                <tr>
                    <td colspan="3" style="padding: 80px 0;"></td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td colspan="2">
            <table class="rules">
                <tr>
                    <td>
                        <p>By purchasing, receiving and using this ticket for an event, you automatically agree to comply with all the rules and instructions of the Organizer installed on the site:</p>
                        <p>1. This ticket is a guarantee of the availability and attendance by you of the West, subject to the observance of the rules and conditions of your visit to the West, as defined by the Charter of the West.</p>
                        <p>2. The purchased return ticket and exchange is not subject to.</p>
                        <p>3. In case of loss and material damage to the ticket, the Ticket Owner is not allowed, the ticket duplicate is not issued, the price of the ticket is not reimbursed.</p>
                        <p>3. In case of loss and material damage to the ticket, the Ticket Owner is not allowed, the ticket duplicate is not issued, the price of the ticket is not reimbursed.</p>
                        <p>5. It is strictly forbidden to visit the West by alcoholic or narcotic intoxication. It is categorically forbidden to carry to the West: any weapon, narcotic, psychotropic substances and means for their use, alcoholic beverages, etc.</p>
                    </td>
                    <td>
                        <p>6. The administration is not responsible for fake tickets, claims for counterfeit tickets are not considered.</p>
                        <p>7. The ticket holder must properly store the ticket before the end of the West, and the ticket may be damaged by the passage. In case if the ticket owner has left the West ahead of schedule, the ticket becomes invalid and can not be returned, a repeated passage to the West on such a ticket is not possible. The holder of an e-ticket must also prevent copying or unauthorized access to the information printed on the ticket, since the right to pass is the one who first presented the ticket during the passage of the control procedure. In case when the buyer has entered personal data (name, serial number and number of the document confirming the identity) during the ordering of the electronic ticket, the primary right of access remains with the person who presented the specified document.</p>
                        <p>8. Your use of the ticket means your unconditional consent to all types of photo and video filming of your person and the public use of these materials by the Organizer without any further consent from you.</p>
                        <p>9. Visitors who violate the established procedure and do not comply with the requirements of Western officials, the security services of the Organizer, will be forced to leave the West without refunding the cost and ticket.</p>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>