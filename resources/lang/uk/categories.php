<?php

return  array(
	'description'   =>  array(
		'9'             => 'Категорія про перегони',
		'10'            => 'Категорія про футбол',
		'11'            => 'Категорія про біг',
		'14'            => 'Категорія про марафон',
	),
	'title'         =>  array(
		'9'             => 'Перегони',
		'10'            => 'Футбол',
		'11'            => 'Біг',
		'14'            => 'Марафон',
	),
);
